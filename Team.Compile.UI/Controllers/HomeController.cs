﻿using System.Web.Mvc;

namespace Team.Compile.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Movie()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Sport()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Travel()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Single()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }
}
